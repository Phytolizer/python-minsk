from typing import Any, Optional

from minsk.code_analysis.compilation import Compilation
from minsk.code_analysis.syntax.syntax_node import SyntaxNode
from minsk.code_analysis.syntax.syntax_tree import SyntaxTree
from minsk.code_analysis.text.text_span import TextSpan
from minsk.code_analysis.variable_symbol import VariableSymbol
from minsk.console import console
from minsk.repl import Repl


class MinskRepl(Repl):
    _variables: dict[VariableSymbol, Any]
    _previous: Optional[Compilation]

    def __init__(self):
        super(MinskRepl, self).__init__()
        self._variables = {}
        self._previous = None
        self._show_tree = False
        self._show_program = False

    def _is_complete_submission(self, text):
        if text == "":
            return False
        syntax_tree = SyntaxTree.parse(text)
        return not SyntaxNode.get_last_token(syntax_tree.root.statement).is_missing()

    def _evaluate_meta_command(self, text):
        if text == "#showTree":
            self._show_tree = not self._show_tree
            if self._show_tree:
                console.print("Showing parse trees.")
            else:
                console.print("Not showing parse trees.")
        elif text == "#showProgram":
            self._show_program = not self._show_program
            if self._show_program:
                console.print("Showing bound tree.")
            else:
                console.print("Not showing bound tree.")
        else:
            super(MinskRepl, self)._evaluate_meta_command(text)

    def _evaluate_submission(self):
        tree = SyntaxTree.parse(self._text)
        if self._show_tree:
            SyntaxNode.pretty_print(tree.root, console)
        if self._previous is None:
            compilation = Compilation(tree)
        else:
            compilation = self._previous.continue_with(tree)
        if self._show_program:
            compilation.emit_tree(console)
        result = compilation.evaluate(self._variables)
        diagnostics = result.diagnostics
        if len(diagnostics) > 0:
            text = tree.text
            print()
            for diagnostic in diagnostics:
                line_index = text.get_line_index(diagnostic.span.start)
                line_number = line_index + 1
                line = text.lines[line_index]
                column_offset = diagnostic.span.start - line.start + 1
                console.print(
                    f"({line_number}, {column_offset}): ", end="", style="red"
                )
                console.print(diagnostic, style="red")
                console.print("\t", end="")

                prefix_span = TextSpan.from_bounds(line.start, diagnostic.span.start)
                suffix_span = TextSpan.from_bounds(diagnostic.span.end, line.end)

                prefix = self._text[prefix_span.start : prefix_span.end]
                error = self._text[diagnostic.span.start : diagnostic.span.end]
                suffix = self._text[suffix_span.start : suffix_span.end]
                console.print(prefix, end="")
                console.print(error, end="", style="red")
                console.print(suffix)
            print()
        else:
            console.print(result.value, style="magenta")
            self._previous = compilation
