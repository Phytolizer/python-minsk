from abc import ABC, abstractmethod

from minsk.console import console


class Repl(ABC):
    def __init__(self):
        self._text = ""

    def run(self):
        """Run the Minsk REPL."""
        while True:
            if len(self._text) == 0:
                console.print("» ", end="", style="green")
            else:
                console.print("· ", end="", style="green")
            try:
                input_line = input()
            except EOFError:
                print()
                return

            if len(self._text) == 0 and input_line.startswith("#"):
                self._evaluate_meta_command(input_line)
                continue
            self._text += f"{input_line}\n"
            if not self._is_complete_submission(self._text):
                continue

            self._evaluate_submission()
            self._text = ""

    @abstractmethod
    def _is_complete_submission(self, text):
        pass

    def _evaluate_meta_command(self, text):
        if text == "#cls":
            console.clear()
        elif text == "#reset":
            self._previous = None
        else:
            console.print(f"Invalid command {text}.")

    @abstractmethod
    def _evaluate_submission(self):
        pass
