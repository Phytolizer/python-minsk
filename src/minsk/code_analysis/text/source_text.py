from minsk.code_analysis.text.text_line import TextLine
from minsk.code_analysis.text.text_span import TextSpan


class SourceText:
    def __init__(self, text):
        self._text = text
        self.lines = self._parse_lines(text)

    def get_line_index(self, position):
        lower = 0
        upper = len(self.lines) - 1

        while lower <= upper:
            index = lower + (upper - lower) // 2
            start = self.lines[index].start

            if position == start:
                return index

            if position > start:
                lower = index + 1
            else:
                upper = index - 1

        return lower - 1

    def __len__(self):
        return len(self._text)

    def __getitem__(self, index):
        if isinstance(index, TextSpan):
            return self._text[index.start : index.end]
        return self._text[index]

    def _parse_lines(self, text) -> tuple[TextLine, ...]:
        lines: list[TextLine] = []

        position = 0
        line_start = 0
        while position < len(text):
            line_break_width = SourceText._get_line_break_width(text, position)
            if line_break_width > 0:
                lines.append(self.add_line(position, line_start, line_break_width))
                position += line_break_width
                line_start = position
            else:
                position += 1
        if position >= line_start:
            lines.append(self.add_line(position, line_start, 0))

        return tuple(lines)

    def add_line(self, position, line_start, line_break_width):
        line_length = position - line_start
        line_length_including_line_break = line_length + line_break_width
        return TextLine(
            self,
            line_start,
            line_length,
            line_length_including_line_break,
        )

    @staticmethod
    def _get_line_break_width(text, position):
        char = text[position]
        look = "\0" if position + 1 >= len(text) else text[position + 1]
        if char == "\r" and look == "\n":
            return 2
        if char in ("\r", "\n"):
            return 1
        return 0
