from minsk.code_analysis.text.text_span import TextSpan


class TextLine:
    def __init__(self, text, start, length, length_including_line_break):
        self.text = text
        self.start = start
        self.length = length
        self.length_including_line_break = length_including_line_break

    @property
    def end(self):
        return self.start + self.length

    @property
    def end_including_line_break(self):
        return self.start + self.length_including_line_break

    @property
    def span(self) -> TextSpan:
        return TextSpan(self.start, self.length)

    @property
    def span_including_line_break(self) -> TextSpan:
        return TextSpan(self.start, self.length_including_line_break)

    def __str__(self):
        return self.text.substring(self.span)
