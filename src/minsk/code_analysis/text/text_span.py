class TextSpan:
    def __init__(self, start, length):
        self.start = start
        self.length = length

    @staticmethod
    def from_bounds(start, end):
        return TextSpan(start, end - start)

    @property
    def end(self):
        return self.start + self.length
