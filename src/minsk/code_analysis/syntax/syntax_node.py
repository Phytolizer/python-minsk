from abc import ABC, abstractmethod
from typing import Generator, Optional

from rich.console import Console

from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.text.text_span import TextSpan


class SyntaxNode(ABC):
    @property
    @abstractmethod
    def kind(self) -> SyntaxKind:
        pass

    @abstractmethod
    def get_children(self) -> Generator["SyntaxNode", None, None]:
        pass

    @property
    def span(self):
        first = self.get_children()[0].span
        last = self.get_children()[-1].span
        return TextSpan.from_bounds(first.start, last.end)

    @staticmethod
    def pretty_print(
        node: "SyntaxNode", console: Console, indent: str = "", is_last: bool = True
    ):
        from minsk.code_analysis.syntax.syntax_token import SyntaxToken

        marker = "└───" if is_last else "├───"
        console.print(f"{indent}{marker}", end="", style="grey53")
        kind_color = "cyan" if isinstance(node, SyntaxToken) else "blue"
        console.print(f"{node.kind.name}", end="", style=kind_color)

        if isinstance(node, SyntaxToken) and node.value is not None:
            console.print(f" [magenta]{node.value}", end="")
        console.print()
        indent += "    " if is_last else "│   "
        children = tuple(node.get_children())
        if children:
            last_child: Optional["SyntaxNode"] = children[-1]
        else:
            last_child = None
        for child in children:
            SyntaxNode.pretty_print(child, console, indent, child == last_child)

    @staticmethod
    def get_last_token(node: "SyntaxNode"):
        from minsk.code_analysis.syntax.syntax_token import SyntaxToken

        if isinstance(node, SyntaxToken):
            return node

        return SyntaxNode.get_last_token(tuple(node.get_children())[-1])
