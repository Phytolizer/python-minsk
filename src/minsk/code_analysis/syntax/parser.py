from minsk.code_analysis.diagnostic_bag import DiagnosticBag
from minsk.code_analysis.syntax import syntax_facts
from minsk.code_analysis.syntax.compilation_unit_syntax import CompilationUnitSyntax
from minsk.code_analysis.syntax.expression_syntax import (
    AssignmentExpressionSyntax,
    BinaryExpressionSyntax,
    LiteralExpressionSyntax,
    NameExpressionSyntax,
    ParenthesizedExpressionSyntax,
    UnaryExpressionSyntax,
)
from minsk.code_analysis.syntax.lexer import Lexer
from minsk.code_analysis.syntax.statement_syntax import (
    BlockStatementSyntax,
    ElseClauseSyntax,
    ExpressionStatementSyntax,
    ForStatementSyntax,
    IfStatementSyntax,
    VariableDeclarationSyntax,
    WhileStatementSyntax,
)
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_token import SyntaxToken


class Parser:
    def __init__(self, text):
        lexer = Lexer(text)
        tokens = []
        while True:
            token = lexer.next_token()
            if token.kind not in {SyntaxKind.WhitespaceToken, SyntaxKind.BadToken}:
                tokens.append(token)
            if token.kind == SyntaxKind.EndOfFileToken:
                break
        self._tokens = tuple(tokens)
        self._position = 0
        self._diagnostics = DiagnosticBag(lexer.diagnostics)

    @property
    def diagnostics(self):
        return tuple(self._diagnostics)

    def _peek(self, offset):
        index = self._position + offset
        if index >= len(self._tokens):
            return self._tokens[-1]
        return self._tokens[index]

    @property
    def _current(self):
        return self._peek(0)

    def _next_token(self):
        current = self._current
        self._position += 1
        return current

    def _match_token(self, kind):
        if self._current.kind == kind:
            return self._next_token()

        self._diagnostics.report_unexpected_token(
            self._current.span, kind.name, self._current.kind.name
        )
        return SyntaxToken(kind, self._current.position, "", None)

    def parse_compilation_unit(self):
        statement = self._parse_statement()
        end_of_file_token = self._match_token(SyntaxKind.EndOfFileToken)
        return CompilationUnitSyntax(statement, end_of_file_token)

    def _parse_statement(self):
        if self._current.kind == SyntaxKind.OpenBraceToken:
            return self._parse_block_statement()
        if self._current.kind in {SyntaxKind.LetKeyword, SyntaxKind.VarKeyword}:
            return self._parse_variable_declaration()
        if self._current.kind == SyntaxKind.IfKeyword:
            return self._parse_if_statement()
        if self._current.kind == SyntaxKind.WhileKeyword:
            return self._parse_while_statement()
        if self._current.kind == SyntaxKind.ForKeyword:
            return self._parse_for_statement()
        return self._parse_expression_statement()

    def _parse_block_statement(self):
        open_brace_token = self._match_token(SyntaxKind.OpenBraceToken)
        statements = []
        start_token = self._current
        while self._current.kind not in {
            SyntaxKind.CloseBraceToken,
            SyntaxKind.EndOfFileToken,
        }:
            statements.append(self._parse_statement())

            if self._current == start_token:
                self._next_token()
            start_token = self._current
        close_brace_token = self._match_token(SyntaxKind.CloseBraceToken)
        return BlockStatementSyntax(
            open_brace_token, tuple(statements), close_brace_token
        )

    def _parse_else_clause(self):
        else_keyword = self._match_token(SyntaxKind.ElseKeyword)
        else_statement = self._parse_statement()
        return ElseClauseSyntax(else_keyword, else_statement)

    def _parse_expression_statement(self):
        expression = self._parse_expression()
        return ExpressionStatementSyntax(expression)

    def _parse_for_statement(self):
        for_keyword = self._match_token(SyntaxKind.ForKeyword)
        identifier_token = self._match_token(SyntaxKind.IdentifierToken)
        equals_token = self._match_token(SyntaxKind.EqualsToken)
        lower_bound = self._parse_expression()
        to_keyword = self._match_token(SyntaxKind.ToKeyword)
        upper_bound = self._parse_expression()
        body = self._parse_statement()
        return ForStatementSyntax(
            for_keyword,
            identifier_token,
            equals_token,
            lower_bound,
            to_keyword,
            upper_bound,
            body,
        )

    def _parse_if_statement(self):
        keyword = self._match_token(SyntaxKind.IfKeyword)
        condition = self._parse_expression()
        then_statement = self._parse_statement()
        if self._current.kind == SyntaxKind.ElseKeyword:
            else_clause = self._parse_else_clause()
        else:
            else_clause = None
        return IfStatementSyntax(keyword, condition, then_statement, else_clause)

    def _parse_variable_declaration(self):
        if self._current.kind == SyntaxKind.LetKeyword:
            expected = SyntaxKind.LetKeyword
        else:
            expected = SyntaxKind.VarKeyword
        keyword = self._match_token(expected)
        identifier = self._match_token(SyntaxKind.IdentifierToken)
        equals_token = self._match_token(SyntaxKind.EqualsToken)
        initializer = self._parse_expression()
        return VariableDeclarationSyntax(keyword, identifier, equals_token, initializer)

    def _parse_while_statement(self):
        keyword = self._match_token(SyntaxKind.WhileKeyword)
        condition = self._parse_expression()
        body = self._parse_statement()
        return WhileStatementSyntax(keyword, condition, body)

    def _parse_expression(self):
        return self._parse_assignment_expression()

    def _parse_assignment_expression(self):
        if (
            self._peek(0).kind == SyntaxKind.IdentifierToken
            and self._peek(1).kind == SyntaxKind.EqualsToken
        ):
            identifier_token = self._next_token()
            equals_token = self._next_token()
            expression = self._parse_assignment_expression()
            return AssignmentExpressionSyntax(
                identifier_token, equals_token, expression
            )
        return self._parse_binary_expression()

    def _parse_binary_expression(self, parent_precedence=0):
        unary_operator_precedence = syntax_facts.unary_operator_precedence(
            self._current.kind
        )
        if (
            unary_operator_precedence != 0
            and unary_operator_precedence >= parent_precedence
        ):
            operator_token = self._next_token()
            operand = self._parse_binary_expression(unary_operator_precedence)
            left = UnaryExpressionSyntax(operator_token, operand)
        else:
            left = self._parse_primary_expression()

        while True:
            precedence = syntax_facts.binary_operator_precedence(self._current.kind)
            if precedence == 0 or precedence <= parent_precedence:
                break
            operator_token = self._next_token()
            right = self._parse_binary_expression(precedence)
            left = BinaryExpressionSyntax(left, operator_token, right)

        return left

    def _parse_primary_expression(self):
        if self._current.kind == SyntaxKind.OpenParenthesisToken:
            return self._parse_parenthesized_expression()
        if self._current.kind in {SyntaxKind.TrueKeyword, SyntaxKind.FalseKeyword}:
            return self._parse_boolean_literal()
        if self._current.kind == SyntaxKind.NumberToken:
            return self._parse_number_literal()
        return self._parse_name_expression()

    def _parse_parenthesized_expression(self):
        open_parenthesis_token = self._match_token(SyntaxKind.OpenParenthesisToken)
        expression = self._parse_expression()
        close_parenthesis_token = self._match_token(SyntaxKind.CloseParenthesisToken)
        return ParenthesizedExpressionSyntax(
            open_parenthesis_token, expression, close_parenthesis_token
        )

    def _parse_boolean_literal(self):
        is_true = self._current.kind == SyntaxKind.TrueKeyword
        if is_true:
            keyword_token = self._match_token(SyntaxKind.TrueKeyword)
        else:
            keyword_token = self._match_token(SyntaxKind.FalseKeyword)
        return LiteralExpressionSyntax(keyword_token, is_true)

    def _parse_number_literal(self):
        literal_token = self._match_token(SyntaxKind.NumberToken)
        return LiteralExpressionSyntax(literal_token)

    def _parse_name_expression(self):
        identifier_token = self._match_token(SyntaxKind.IdentifierToken)
        return NameExpressionSyntax(identifier_token)
