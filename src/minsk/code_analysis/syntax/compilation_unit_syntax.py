from dataclasses import dataclass

from minsk.code_analysis.syntax.statement_syntax import StatementSyntax
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_node import SyntaxNode
from minsk.code_analysis.syntax.syntax_token import SyntaxToken


@dataclass
class CompilationUnitSyntax(SyntaxNode):
    statement: StatementSyntax
    end_of_file_token: SyntaxToken

    @property
    def kind(self):
        return SyntaxKind.CompilationUnit

    def get_children(self):
        yield self.statement
        yield self.end_of_file_token
