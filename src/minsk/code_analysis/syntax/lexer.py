from minsk.code_analysis.diagnostic_bag import DiagnosticBag
from minsk.code_analysis.syntax import syntax_facts
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_token import SyntaxToken
from minsk.code_analysis.text.text_span import TextSpan


class Lexer:
    def __init__(self, text):
        self._text = text
        self._position = 0
        self._diagnostics = DiagnosticBag()

    def _peek(self, offset):
        index = self._position + offset
        if index >= len(self._text):
            return "\0"
        else:
            return self._text[index]

    @property
    def _current(self):
        return self._peek(0)

    @property
    def _lookahead(self):
        return self._peek(1)

    @property
    def diagnostics(self):
        return tuple(self._diagnostics)

    def next_token(self):
        self._start = self._position
        self._kind = SyntaxKind.BadToken
        self._value = None

        if self._current == "\0":
            self._kind = SyntaxKind.EndOfFileToken
        elif self._current.isdigit():
            self._read_number_token()
        elif self._current.isalpha():
            self._read_identifier_or_keyword()
        elif self._current.isspace():
            self._read_whitespace_token()
        elif self._current == "&" and self._lookahead == "&":
            self._position += 2
            self._kind = SyntaxKind.AmpersandAmpersandToken
        elif self._current == "|" and self._lookahead == "|":
            self._position += 2
            self._kind = SyntaxKind.PipePipeToken
        elif self._current == "=" and self._lookahead == "=":
            self._position += 2
            self._kind = SyntaxKind.EqualsEqualsToken
        elif self._current == "!" and self._lookahead == "=":
            self._position += 2
            self._kind = SyntaxKind.BangEqualsToken
        elif self._current == "<" and self._lookahead == "=":
            self._position += 2
            self._kind = SyntaxKind.LessOrEqualsToken
        elif self._current == ">" and self._lookahead == "=":
            self._position += 2
            self._kind = SyntaxKind.GreaterOrEqualsToken
        elif self._current == "<":
            self._position += 1
            self._kind = SyntaxKind.LessToken
        elif self._current == ">":
            self._position += 1
            self._kind = SyntaxKind.GreaterToken
        elif self._current == "|":
            self._position += 1
            self._kind = SyntaxKind.PipeToken
        elif self._current == "&":
            self._position += 1
            self._kind = SyntaxKind.AmpersandToken
        elif self._current == "^":
            self._position += 1
            self._kind = SyntaxKind.HatToken
        elif self._current == "~":
            self._position += 1
            self._kind = SyntaxKind.TildeToken
        elif self._current == "+":
            self._position += 1
            self._kind = SyntaxKind.PlusToken
        elif self._current == "-":
            self._position += 1
            self._kind = SyntaxKind.MinusToken
        elif self._current == "*":
            self._position += 1
            self._kind = SyntaxKind.StarToken
        elif self._current == "/":
            self._position += 1
            self._kind = SyntaxKind.SlashToken
        elif self._current == "!":
            self._position += 1
            self._kind = SyntaxKind.BangToken
        elif self._current == "(":
            self._position += 1
            self._kind = SyntaxKind.OpenParenthesisToken
        elif self._current == ")":
            self._position += 1
            self._kind = SyntaxKind.CloseParenthesisToken
        elif self._current == "{":
            self._position += 1
            self._kind = SyntaxKind.OpenBraceToken
        elif self._current == "}":
            self._position += 1
            self._kind = SyntaxKind.CloseBraceToken
        elif self._current == "=":
            self._position += 1
            self._kind = SyntaxKind.EqualsToken
        else:
            self._diagnostics.report_bad_character(self._position, self._current)
            self._position += 1
            self._kind = SyntaxKind.BadToken

        return SyntaxToken(
            self._kind,
            self._start,
            self._text[self._start : self._position],
            self._value,
        )

    def _read_whitespace_token(self):
        while self._current.isspace():
            self._position += 1
        self._kind = SyntaxKind.WhitespaceToken

    def _read_identifier_or_keyword(self):
        while self._current.isalpha():
            self._position += 1
        text = self._text[self._start : self._position]
        self._kind = syntax_facts.keyword_kind(text)

    def _read_number_token(self):
        while self._current.isdigit():
            self._position += 1
        text = self._text[self._start : self._position]
        try:
            self._value = int(text)
        except ValueError:
            self._diagnostics.report_invalid_integer(
                TextSpan.from_bounds(self._start, self._position), text
            )
        self._kind = SyntaxKind.NumberToken
