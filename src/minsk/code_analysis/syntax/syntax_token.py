from dataclasses import dataclass
from typing import Any

from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_node import SyntaxNode
from minsk.code_analysis.text.text_span import TextSpan


@dataclass
class SyntaxToken(SyntaxNode):
    _kind: SyntaxKind
    position: int
    text: str
    value: Any

    @property
    def kind(self):
        return self._kind

    @property
    def span(self):
        return TextSpan(self.position, len(self.text))

    def get_children(self):
        return iter(())

    def is_missing(self):
        return len(self.text) == 0
