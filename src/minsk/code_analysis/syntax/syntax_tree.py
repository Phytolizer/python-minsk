from typing import Union

from minsk.code_analysis.syntax.compilation_unit_syntax import CompilationUnitSyntax
from minsk.code_analysis.syntax.lexer import Lexer
from minsk.code_analysis.syntax.parser import Parser
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_token import SyntaxToken
from minsk.code_analysis.text.source_text import SourceText


class SyntaxTree:
    root: CompilationUnitSyntax
    diagnostics: tuple[str]
    text: SourceText

    def __init__(self, text: SourceText):
        parser = Parser(text)
        root = parser.parse_compilation_unit()
        diagnostics = parser.diagnostics

        self.root = root
        self.diagnostics = diagnostics
        self.text = text

    @staticmethod
    def parse(text: Union[str, SourceText]) -> "SyntaxTree":
        if isinstance(text, str):
            text = SourceText(text)
        return SyntaxTree(text)

    @staticmethod
    def parse_tokens(text: Union[str, SourceText]) -> tuple[SyntaxToken, ...]:
        if isinstance(text, str):
            text = SourceText(text)
        lexer = Lexer(text)
        tokens = []
        while True:
            token = lexer.next_token()
            if token.kind == SyntaxKind.EndOfFileToken:
                break
            tokens.append(token)
        return tuple(tokens)
