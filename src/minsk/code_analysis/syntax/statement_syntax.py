from typing import Optional

from minsk.code_analysis.syntax.expression_syntax import ExpressionSyntax
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_node import SyntaxNode
from minsk.code_analysis.syntax.syntax_token import SyntaxToken


class StatementSyntax(SyntaxNode):
    pass


class BlockStatementSyntax(StatementSyntax):
    def __init__(
        self,
        open_brace_token: SyntaxToken,
        statements: tuple[StatementSyntax],
        close_brace_token: SyntaxToken,
    ):
        self.open_brace_token = open_brace_token
        self.statements = statements
        self.close_brace_token = close_brace_token

    @property
    def kind(self):
        return SyntaxKind.BlockStatement

    def get_children(self):
        yield self.open_brace_token
        yield from self.statements
        yield self.close_brace_token


class ElseClauseSyntax(SyntaxNode):
    def __init__(self, else_keyword: SyntaxToken, else_statement: StatementSyntax):
        self.else_keyword = else_keyword
        self.else_statement = else_statement

    @property
    def kind(self):
        return SyntaxKind.ElseClause

    def get_children(self):
        yield self.else_keyword
        yield self.else_statement


class ExpressionStatementSyntax(StatementSyntax):
    def __init__(self, expression: ExpressionSyntax):
        self.expression = expression

    @property
    def kind(self):
        return SyntaxKind.ExpressionStatement

    def get_children(self):
        yield self.expression


class ForStatementSyntax(StatementSyntax):
    def __init__(
        self,
        for_keyword: SyntaxToken,
        identifier_token: SyntaxToken,
        equals_token: SyntaxToken,
        lower_bound: ExpressionSyntax,
        to_keyword: SyntaxToken,
        upper_bound: ExpressionSyntax,
        body: StatementSyntax,
    ):
        self.for_keyword = for_keyword
        self.identifier_token = identifier_token
        self.equals_token = equals_token
        self.lower_bound = lower_bound
        self.to_keyword = to_keyword
        self.upper_bound = upper_bound
        self.body = body

    @property
    def kind(self):
        return SyntaxKind.ForStatement

    def get_children(self):
        yield self.for_keyword
        yield self.identifier_token
        yield self.equals_token
        yield self.lower_bound
        yield self.to_keyword
        yield self.upper_bound


class IfStatementSyntax(StatementSyntax):
    def __init__(
        self,
        if_keyword: SyntaxToken,
        condition: ExpressionSyntax,
        then_statement: StatementSyntax,
        else_clause: Optional[ElseClauseSyntax],
    ):
        self.if_keyword = if_keyword
        self.condition = condition
        self.then_statement = then_statement
        self.else_clause = else_clause

    @property
    def kind(self):
        return SyntaxKind.IfStatement

    def get_children(self):
        yield self.if_keyword
        yield self.condition
        yield self.then_statement
        if self.else_clause is not None:
            yield self.else_clause


class VariableDeclarationSyntax(StatementSyntax):
    def __init__(
        self,
        keyword: SyntaxToken,
        identifier: SyntaxToken,
        equals_token: SyntaxToken,
        initializer: ExpressionSyntax,
    ):
        self.keyword = keyword
        self.identifier = identifier
        self.equals_token = equals_token
        self.initializer = initializer

    @property
    def kind(self):
        return SyntaxKind.VariableDeclaration

    def get_children(self):
        yield self.keyword
        yield self.identifier
        yield self.equals_token
        yield self.initializer


class WhileStatementSyntax(StatementSyntax):
    def __init__(
        self,
        keyword: SyntaxToken,
        condition: ExpressionSyntax,
        body: StatementSyntax,
    ):
        self.keyword = keyword
        self.condition = condition
        self.body = body

    @property
    def kind(self):
        return SyntaxKind.WhileStatement

    def get_children(self):
        yield self.keyword
        yield self.condition
        yield self.body
