from typing import Generator, Optional

from minsk.code_analysis.syntax.syntax_kind import SyntaxKind


def binary_operator_precedence(kind: SyntaxKind) -> int:
    if kind in {SyntaxKind.StarToken, SyntaxKind.SlashToken}:
        return 5
    if kind in {SyntaxKind.PlusToken, SyntaxKind.MinusToken}:
        return 4
    if kind in {
        SyntaxKind.EqualsEqualsToken,
        SyntaxKind.BangEqualsToken,
        SyntaxKind.LessOrEqualsToken,
        SyntaxKind.GreaterOrEqualsToken,
        SyntaxKind.LessToken,
        SyntaxKind.GreaterToken,
    }:
        return 3
    if kind in {SyntaxKind.AmpersandAmpersandToken, SyntaxKind.AmpersandToken}:
        return 2
    if kind in {SyntaxKind.PipePipeToken, SyntaxKind.PipeToken, SyntaxKind.HatToken}:
        return 1
    return 0


def unary_operator_precedence(kind: SyntaxKind) -> int:
    if kind in {
        SyntaxKind.PlusToken,
        SyntaxKind.MinusToken,
        SyntaxKind.BangToken,
        SyntaxKind.TildeToken,
    }:
        return 6
    return 0


def keyword_kind(text: str) -> SyntaxKind:
    return {
        "else": SyntaxKind.ElseKeyword,
        "false": SyntaxKind.FalseKeyword,
        "for": SyntaxKind.ForKeyword,
        "if": SyntaxKind.IfKeyword,
        "let": SyntaxKind.LetKeyword,
        "to": SyntaxKind.ToKeyword,
        "true": SyntaxKind.TrueKeyword,
        "var": SyntaxKind.VarKeyword,
        "while": SyntaxKind.WhileKeyword,
    }.get(text, SyntaxKind.IdentifierToken)


def get_text(kind: SyntaxKind) -> Optional[str]:
    return {
        SyntaxKind.PlusToken: "+",
        SyntaxKind.MinusToken: "-",
        SyntaxKind.StarToken: "*",
        SyntaxKind.SlashToken: "/",
        SyntaxKind.BangToken: "!",
        SyntaxKind.EqualsEqualsToken: "==",
        SyntaxKind.EqualsToken: "=",
        SyntaxKind.BangEqualsToken: "!=",
        SyntaxKind.LessOrEqualsToken: "<=",
        SyntaxKind.GreaterOrEqualsToken: ">=",
        SyntaxKind.LessToken: "<",
        SyntaxKind.GreaterToken: ">",
        SyntaxKind.PipeToken: "|",
        SyntaxKind.AmpersandToken: "&",
        SyntaxKind.HatToken: "^",
        SyntaxKind.TildeToken: "~",
        SyntaxKind.AmpersandAmpersandToken: "&&",
        SyntaxKind.PipePipeToken: "||",
        SyntaxKind.OpenParenthesisToken: "(",
        SyntaxKind.CloseParenthesisToken: ")",
        SyntaxKind.OpenBraceToken: "{",
        SyntaxKind.CloseBraceToken: "}",
        SyntaxKind.ElseKeyword: "else",
        SyntaxKind.FalseKeyword: "false",
        SyntaxKind.ForKeyword: "for",
        SyntaxKind.IfKeyword: "if",
        SyntaxKind.LetKeyword: "let",
        SyntaxKind.ToKeyword: "to",
        SyntaxKind.TrueKeyword: "true",
        SyntaxKind.VarKeyword: "var",
        SyntaxKind.WhileKeyword: "while",
    }.get(kind, None)


def get_binary_operators() -> Generator[SyntaxKind, None, None]:
    for kind in SyntaxKind:
        if binary_operator_precedence(kind) > 0:
            yield kind


def get_unary_operators() -> Generator[SyntaxKind, None, None]:
    for kind in SyntaxKind:
        if unary_operator_precedence(kind) > 0:
            yield kind
