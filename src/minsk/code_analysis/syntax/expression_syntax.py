from abc import ABC

from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_node import SyntaxNode


class ExpressionSyntax(SyntaxNode, ABC):
    pass


class AssignmentExpressionSyntax(ExpressionSyntax):
    def __init__(self, identifier_token, equals_token, expression):
        self.identifier_token = identifier_token
        self.equals_token = equals_token
        self.expression = expression

    @property
    def kind(self):
        return SyntaxKind.AssignmentExpression

    def get_children(self):
        yield self.identifier_token
        yield self.equals_token
        yield self.expression


class BinaryExpressionSyntax(ExpressionSyntax):
    def __init__(self, left, operator_token, right):
        self.left = left
        self.operator_token = operator_token
        self.right = right

    @property
    def kind(self):
        return SyntaxKind.BinaryExpression

    def get_children(self):
        yield self.left
        yield self.operator_token
        yield self.right


class LiteralExpressionSyntax(ExpressionSyntax):
    def __init__(self, literal_token, value=None):
        self.literal_token = literal_token
        if value is None:
            value = literal_token.value
        self.value = value

    @property
    def kind(self):
        return SyntaxKind.LiteralExpression

    def get_children(self):
        return (self.literal_token,)


class NameExpressionSyntax(ExpressionSyntax):
    def __init__(self, identifier_token):
        self.identifier_token = identifier_token

    @property
    def kind(self):
        return SyntaxKind.NameExpression

    def get_children(self):
        yield self.identifier_token


class ParenthesizedExpressionSyntax(ExpressionSyntax):
    def __init__(self, open_parenthesis_token, expression, close_parenthesis_token):
        self.open_parenthesis_token = open_parenthesis_token
        self.expression = expression
        self.close_parenthesis_token = close_parenthesis_token

    @property
    def kind(self):
        return SyntaxKind.ParenthesizedExpression

    def get_children(self):
        yield self.open_parenthesis_token
        yield self.expression
        yield self.close_parenthesis_token


class UnaryExpressionSyntax(ExpressionSyntax):
    def __init__(self, operator_token, operand):
        self.operator_token = operator_token
        self.operand = operand

    @property
    def kind(self):
        return SyntaxKind.UnaryExpression

    def get_children(self):
        yield self.operator_token
        yield self.operand
