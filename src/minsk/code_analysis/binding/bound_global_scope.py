from typing import Optional

from minsk.code_analysis.binding.bound_statement import BoundStatement
from minsk.code_analysis.diagnostic import Diagnostic
from minsk.code_analysis.variable_symbol import VariableSymbol


class BoundGlobalScope:
    def __init__(
        self,
        previous: Optional["BoundGlobalScope"],
        diagnostics: tuple[Diagnostic, ...],
        variables: tuple[VariableSymbol, ...],
        statement: BoundStatement,
    ):
        self.previous = previous
        self.diagnostics = diagnostics
        self.variables = variables
        self.statement = statement
