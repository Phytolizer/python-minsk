from enum import Enum, auto
from typing import Optional, Type

from minsk.code_analysis.syntax.syntax_kind import SyntaxKind


class BoundBinaryOperatorKind(Enum):
    Addition = auto()
    Subtraction = auto()
    Multiplication = auto()
    Division = auto()
    LogicalAnd = auto()
    LogicalOr = auto()
    Equality = auto()
    Inequality = auto()
    LessOrEquals = auto()
    GreaterOrEquals = auto()
    Less = auto()
    Greater = auto()
    BitwiseAnd = auto()
    BitwiseOr = auto()
    BitwiseXor = auto()


class BoundBinaryOperator:
    def __init__(
        self,
        syntax_kind: SyntaxKind,
        kind: BoundBinaryOperatorKind,
        left_type: Type,
        right_type: Type = None,
        result_type: Type = None,
    ):
        self.syntax_kind = syntax_kind
        self.kind = kind
        self.left_type = left_type
        if right_type is None and result_type is None:
            right_type = left_type
            result_type = left_type
        elif result_type is None:
            result_type = right_type
            right_type = left_type
        self.right_type = right_type
        self.result_type = result_type

    @staticmethod
    def bind(
        syntax_kind: SyntaxKind, left_type: Type, right_type: Type
    ) -> Optional["BoundBinaryOperator"]:
        for op in _BOUND_BINARY_OPERATORS:
            if (
                op.syntax_kind == syntax_kind
                and op.left_type == left_type
                and op.right_type == right_type
            ):
                return op
        return None


_BOUND_BINARY_OPERATORS = (
    BoundBinaryOperator(SyntaxKind.PlusToken, BoundBinaryOperatorKind.Addition, int),
    BoundBinaryOperator(
        SyntaxKind.MinusToken, BoundBinaryOperatorKind.Subtraction, int
    ),
    BoundBinaryOperator(
        SyntaxKind.StarToken, BoundBinaryOperatorKind.Multiplication, int
    ),
    BoundBinaryOperator(SyntaxKind.SlashToken, BoundBinaryOperatorKind.Division, int),
    BoundBinaryOperator(
        SyntaxKind.AmpersandAmpersandToken, BoundBinaryOperatorKind.LogicalAnd, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.PipePipeToken, BoundBinaryOperatorKind.LogicalOr, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.EqualsEqualsToken, BoundBinaryOperatorKind.Equality, int, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.EqualsEqualsToken, BoundBinaryOperatorKind.Equality, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.BangEqualsToken, BoundBinaryOperatorKind.Inequality, int, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.BangEqualsToken, BoundBinaryOperatorKind.Inequality, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.LessOrEqualsToken, BoundBinaryOperatorKind.LessOrEquals, int, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.GreaterOrEqualsToken,
        BoundBinaryOperatorKind.GreaterOrEquals,
        int,
        bool,
    ),
    BoundBinaryOperator(SyntaxKind.LessToken, BoundBinaryOperatorKind.Less, int, bool),
    BoundBinaryOperator(
        SyntaxKind.GreaterToken, BoundBinaryOperatorKind.Greater, int, bool
    ),
    BoundBinaryOperator(
        SyntaxKind.AmpersandToken, BoundBinaryOperatorKind.BitwiseAnd, int
    ),
    BoundBinaryOperator(
        SyntaxKind.AmpersandToken, BoundBinaryOperatorKind.BitwiseAnd, bool
    ),
    BoundBinaryOperator(SyntaxKind.PipeToken, BoundBinaryOperatorKind.BitwiseOr, int),
    BoundBinaryOperator(SyntaxKind.PipeToken, BoundBinaryOperatorKind.BitwiseOr, bool),
    BoundBinaryOperator(SyntaxKind.HatToken, BoundBinaryOperatorKind.BitwiseXor, int),
    BoundBinaryOperator(SyntaxKind.HatToken, BoundBinaryOperatorKind.BitwiseXor, bool),
)


class BoundUnaryOperatorKind(Enum):
    Identity = auto()
    Negation = auto()
    LogicalNegation = auto()
    BitwiseNegation = auto()


class BoundUnaryOperator:
    def __init__(
        self,
        syntax_kind: SyntaxKind,
        kind: BoundUnaryOperatorKind,
        operand_type: Type,
        result_type: Type = None,
    ):
        self.syntax_kind = syntax_kind
        self.kind = kind
        self.operand_type = operand_type
        if result_type is None:
            result_type = operand_type
        self.result_type = result_type

    @staticmethod
    def bind(
        syntax_kind: SyntaxKind, operand_type: Type
    ) -> Optional["BoundUnaryOperator"]:
        for op in _BOUND_UNARY_OPERATORS:
            if op.syntax_kind == syntax_kind and op.operand_type == operand_type:
                return op
        return None


_BOUND_UNARY_OPERATORS = (
    BoundUnaryOperator(SyntaxKind.PlusToken, BoundUnaryOperatorKind.Identity, int),
    BoundUnaryOperator(SyntaxKind.MinusToken, BoundUnaryOperatorKind.Negation, int),
    BoundUnaryOperator(
        SyntaxKind.BangToken, BoundUnaryOperatorKind.LogicalNegation, bool
    ),
    BoundUnaryOperator(
        SyntaxKind.TildeToken, BoundUnaryOperatorKind.BitwiseNegation, int
    ),
)
