from enum import Enum, auto


class BoundNodeKind(Enum):
    BlockStatement = auto()
    ConditionalGotoStatement = auto()
    ExpressionStatement = auto()
    ForStatement = auto()
    GotoStatement = auto()
    IfStatement = auto()
    LabelStatement = auto()
    VariableDeclaration = auto()
    WhileStatement = auto()

    AssignmentExpression = auto()
    BinaryExpression = auto()
    LiteralExpression = auto()
    UnaryExpression = auto()
    VariableExpression = auto()
