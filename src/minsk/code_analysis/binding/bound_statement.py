from typing import Optional

from minsk.code_analysis.binding.bound_expression import BoundExpression
from minsk.code_analysis.binding.bound_node import BoundNode
from minsk.code_analysis.binding.bound_node_kind import BoundNodeKind
from minsk.code_analysis.label_symbol import LabelSymbol
from minsk.code_analysis.variable_symbol import VariableSymbol


class BoundStatement(BoundNode):
    pass


class BoundBlockStatement(BoundStatement):
    def __init__(self, statements: tuple[BoundStatement, ...]):
        self.statements = statements

    @property
    def kind(self):
        return BoundNodeKind.BlockStatement


class BoundConditionalGotoStatement(BoundStatement):
    def __init__(
        self, label: LabelSymbol, condition: BoundExpression, jump_if_true: bool
    ):
        self.label = label
        self.condition = condition
        self.jump_if_true = jump_if_true

    @property
    def kind(self):
        return BoundNodeKind.ConditionalGotoStatement


class BoundExpressionStatement(BoundStatement):
    def __init__(self, expression: BoundExpression):
        self.expression = expression

    @property
    def kind(self):
        return BoundNodeKind.ExpressionStatement


class BoundForStatement(BoundStatement):
    def __init__(
        self,
        variable: VariableSymbol,
        lower_bound: BoundExpression,
        upper_bound: BoundExpression,
        body: BoundStatement,
    ):
        self.variable = variable
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.body = body

    @property
    def kind(self):
        return BoundNodeKind.ForStatement


class BoundGotoStatement(BoundStatement):
    def __init__(self, label: LabelSymbol):
        self.label = label

    @property
    def kind(self):
        return BoundNodeKind.GotoStatement


class BoundIfStatement(BoundStatement):
    def __init__(
        self,
        condition: BoundExpression,
        then_statement: BoundStatement,
        else_statement: Optional[BoundStatement],
    ):
        self.condition = condition
        self.then_statement = then_statement
        self.else_statement = else_statement

    @property
    def kind(self):
        return BoundNodeKind.IfStatement


class BoundLabelStatement(BoundStatement):
    def __init__(self, label: LabelSymbol):
        self.label = label

    @property
    def kind(self):
        return BoundNodeKind.LabelStatement


class BoundVariableDeclaration(BoundStatement):
    def __init__(self, variable: VariableSymbol, initializer: BoundExpression):
        self.variable = variable
        self.initializer = initializer

    @property
    def kind(self):
        return BoundNodeKind.VariableDeclaration


class BoundWhileStatement(BoundStatement):
    def __init__(self, condition: BoundExpression, body: BoundStatement):
        self.condition = condition
        self.body = body

    @property
    def kind(self):
        return BoundNodeKind.WhileStatement
