from abc import ABC, abstractmethod
from inspect import ismethod, signature
from typing import Generator, Optional, Type

from minsk.code_analysis.binding.bound_node_kind import BoundNodeKind


class BoundNode(ABC):
    @property
    @abstractmethod
    def kind(self) -> BoundNodeKind:
        pass

    def get_children(self) -> Generator[tuple[str, "BoundNode"], None, None]:
        init_method = getattr(self, "__init__")
        sig = signature(init_method)
        for param in sig.parameters.values():
            value = getattr(self, param.name)
            if isinstance(value, tuple):
                for child in value:
                    yield (param.name, child)
            else:
                yield (param.name, value)

    def pretty_print(self, console):
        BoundNode._pretty_print(self, console)

    @staticmethod
    def _get_properties(node):
        for prop in dir(node):
            if prop.startswith("_"):
                continue
            if prop in {"kind", "operator"}:
                continue
            pval = getattr(node, prop)
            if ismethod(pval):
                continue
            if isinstance(pval, BoundNode):
                continue
            if isinstance(pval, tuple):
                continue
            if isinstance(pval, Type):
                pval = pval.__name__
            yield (prop, pval)

    @staticmethod
    def _get_text(node):
        from minsk.code_analysis.binding.bound_expression import (
            BoundBinaryExpression,
            BoundUnaryExpression,
        )

        if isinstance(node, BoundBinaryExpression):
            return f"{node.operator.kind.name}Expression"
        if isinstance(node, BoundUnaryExpression):
            return f"{node.operator.kind.name}Expression"

        return node.kind.name

    @staticmethod
    def _get_style(node):
        from minsk.code_analysis.binding.bound_expression import BoundExpression
        from minsk.code_analysis.binding.bound_statement import BoundStatement

        if isinstance(node, BoundExpression):
            return "blue"
        if isinstance(node, BoundStatement):
            return "cyan"
        return "yellow"

    @staticmethod
    def _pretty_print(node, console, indent="", is_last=True):
        marker = "└───" if is_last else "├───"
        console.print(f"{indent}{marker}", end="", style="grey53")
        style = BoundNode._get_style(node)
        text = BoundNode._get_text(node)
        console.print(text, end="", style=style)
        is_first_property = True
        for p in BoundNode._get_properties(node):
            if is_first_property:
                is_first_property = False
            else:
                console.print(",", end="", style="grey53")
            console.print(" ", end="")
            console.print(p[0], end="", style="bright_yellow")
            console.print(" = ", end="", style="grey53")
            console.print(p[1], end="", style="yellow")

        console.print()
        indent += "    " if is_last else "│   "
        children = tuple(node.get_children())
        if children:
            last_child: Optional["BoundNode"] = children[-1]
        else:
            last_child = None
        for child in children:
            if isinstance(child[1], BoundNode):
                BoundNode._pretty_print(child[1], console, indent, child == last_child)
