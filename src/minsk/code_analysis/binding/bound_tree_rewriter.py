from abc import ABC

from minsk.code_analysis.binding.bound_expression import (
    BoundAssignmentExpression,
    BoundBinaryExpression,
    BoundExpression,
    BoundUnaryExpression,
)
from minsk.code_analysis.binding.bound_node_kind import BoundNodeKind
from minsk.code_analysis.binding.bound_statement import (
    BoundBlockStatement,
    BoundConditionalGotoStatement,
    BoundExpressionStatement,
    BoundForStatement,
    BoundIfStatement,
    BoundStatement,
    BoundVariableDeclaration,
    BoundWhileStatement,
)


class BoundTreeRewriter(ABC):
    def rewrite_statement(self, node: BoundStatement) -> BoundStatement:
        if node.kind == BoundNodeKind.BlockStatement:
            return self.rewrite_block_statement(node)
        if node.kind == BoundNodeKind.ConditionalGotoStatement:
            return self.rewrite_conditional_goto_statement(node)
        if node.kind == BoundNodeKind.ExpressionStatement:
            return self.rewrite_expression_statement(node)
        if node.kind == BoundNodeKind.ForStatement:
            return self.rewrite_for_statement(node)
        if node.kind == BoundNodeKind.GotoStatement:
            return self.rewrite_goto_statement(node)
        if node.kind == BoundNodeKind.IfStatement:
            return self.rewrite_if_statement(node)
        if node.kind == BoundNodeKind.LabelStatement:
            return self.rewrite_label_statement(node)
        if node.kind == BoundNodeKind.VariableDeclaration:
            return self.rewrite_variable_declaration(node)
        if node.kind == BoundNodeKind.WhileStatement:
            return self.rewrite_while_statement(node)
        raise Exception(f"unexpected node {node.kind.name}")

    def rewrite_block_statement(self, node):
        statements = None
        for i in range(len(node.statements)):
            old_statement = node.statements[i]
            statement = self.rewrite_statement(old_statement)
            if statement != old_statement and statements is None:
                statements = [node.statements[j] for j in range(i)]
                statements.append(statement)
            elif statements is not None:
                statements.append(statement)
        if statements is None:
            return node
        return BoundBlockStatement(statements)

    def rewrite_conditional_goto_statement(self, node):
        condition = self.rewrite_expression(node.condition)
        if condition == node.condition:
            return node
        return BoundConditionalGotoStatement(node.label, condition, node.jump_if_false)

    def rewrite_expression_statement(self, node):
        expression = self.rewrite_expression(node.expression)
        if expression == node.expression:
            return node
        return BoundExpressionStatement(expression)

    def rewrite_for_statement(self, node):
        lower_bound = self.rewrite_expression(node.lower_bound)
        upper_bound = self.rewrite_expression(node.upper_bound)
        body = self.rewrite_statement(node.body)
        if (
            lower_bound == node.lower_bound
            and upper_bound == node.upper_bound
            and body == node.body
        ):
            return node
        return BoundForStatement(node.variable, lower_bound, upper_bound, body)

    def rewrite_goto_statement(self, node):
        return node

    def rewrite_if_statement(self, node):
        condition = self.rewrite_expression(node.condition)
        then_statement = self.rewrite_statement(node.then_statement)
        if node.else_statement is not None:
            else_statement = self.rewrite_statement(node.else_statement)
        else:
            else_statement = None
        if (
            condition == node.condition
            and then_statement == node.then_statement
            and else_statement == node.else_statement
        ):
            return node
        return BoundIfStatement(condition, then_statement, else_statement)

    def rewrite_label_statement(self, node):
        return node

    def rewrite_variable_declaration(self, node):
        initializer = self.rewrite_expression(node.initializer)
        if initializer == node.initializer:
            return node
        return BoundVariableDeclaration(node.variable, initializer)

    def rewrite_while_statement(self, node):
        condition = self.rewrite_expression(node.condition)
        body = self.rewrite_statement(node.body)
        if condition == node.condition and body == node.body:
            return node
        return BoundWhileStatement(condition, body)

    def rewrite_expression(self, node: BoundExpression) -> BoundExpression:
        if node.kind == BoundNodeKind.AssignmentExpression:
            return self.rewrite_assignment_expression(node)
        if node.kind == BoundNodeKind.BinaryExpression:
            return self.rewrite_variable_expression(node)
        if node.kind == BoundNodeKind.LiteralExpression:
            return self.rewrite_literal_expression(node)
        if node.kind == BoundNodeKind.UnaryExpression:
            return self.rewrite_unary_expression(node)
        if node.kind == BoundNodeKind.VariableExpression:
            return self.rewrite_variable_expression(node)
        raise Exception(f"unexpected node {node.kind.name}")

    def rewrite_assignment_expression(self, node):
        expression = self.rewrite_expression(node.expression)
        if expression == node.expression:
            return node
        return BoundAssignmentExpression(node.variable, expression)

    def rewrite_binary_expression(self, node):
        left = self.rewrite_expression(node.left)
        right = self.rewrite_expression(node.right)
        if left == node.left and right == node.right:
            return node
        return BoundBinaryExpression(left, node.operator, right)

    def rewrite_literal_expression(self, node):
        return node

    def rewrite_unary_expression(self, node):
        operand = self.rewrite_expression(node.operand)
        if operand == node.operand:
            return node
        return BoundUnaryExpression(node.operator, operand)

    def rewrite_variable_expression(self, node):
        return node
