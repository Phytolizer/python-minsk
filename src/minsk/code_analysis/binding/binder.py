from typing import Optional, Type, cast

from minsk.code_analysis.binding.bound_expression import (
    BoundAssignmentExpression,
    BoundBinaryExpression,
    BoundExpression,
    BoundLiteralExpression,
    BoundUnaryExpression,
    BoundVariableExpression,
)
from minsk.code_analysis.binding.bound_global_scope import BoundGlobalScope
from minsk.code_analysis.binding.bound_scope import BoundScope
from minsk.code_analysis.binding.bound_statement import (
    BoundBlockStatement,
    BoundExpressionStatement,
    BoundForStatement,
    BoundIfStatement,
    BoundStatement,
    BoundVariableDeclaration,
    BoundWhileStatement,
)
from minsk.code_analysis.binding.operators import (
    BoundBinaryOperator,
    BoundUnaryOperator,
)
from minsk.code_analysis.diagnostic_bag import DiagnosticBag
from minsk.code_analysis.syntax.compilation_unit_syntax import CompilationUnitSyntax
from minsk.code_analysis.syntax.expression_syntax import ExpressionSyntax
from minsk.code_analysis.syntax.statement_syntax import StatementSyntax
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.variable_symbol import VariableSymbol


class Binder:
    _scope: BoundScope

    def __init__(self, parent: Optional[BoundScope]):
        self.diagnostics = DiagnosticBag()
        self._scope = BoundScope(parent)

    @staticmethod
    def bind_global_scope(
        previous: Optional[BoundGlobalScope], syntax: CompilationUnitSyntax
    ) -> BoundGlobalScope:
        parent_scope = Binder._create_parent_scopes(previous)
        binder = Binder(parent_scope)
        statement = binder._bind_statement(syntax.statement)
        variables = binder._scope.get_declared_variables()
        diagnostics = tuple(binder.diagnostics)
        return BoundGlobalScope(None, diagnostics, tuple(variables), statement)

    @staticmethod
    def _create_parent_scopes(
        previous: Optional[BoundGlobalScope],
    ) -> Optional[BoundScope]:
        stack = []
        while previous is not None:
            stack.append(previous)
            previous = previous.previous

        parent: Optional[BoundScope] = None

        while stack:
            glob = stack.pop()
            scope = BoundScope(parent)
            for v in glob.variables:
                scope.try_declare(v)
            parent = scope

        return parent

    def _bind_statement(self, syntax: StatementSyntax) -> BoundStatement:
        if syntax.kind == SyntaxKind.BlockStatement:
            return self._bind_block_statement(syntax)
        if syntax.kind == SyntaxKind.ExpressionStatement:
            return self._bind_expression_statement(syntax)
        if syntax.kind == SyntaxKind.ForStatement:
            return self._bind_for_statement(syntax)
        if syntax.kind == SyntaxKind.IfStatement:
            return self._bind_if_statement(syntax)
        if syntax.kind == SyntaxKind.VariableDeclaration:
            return self._bind_variable_declaration(syntax)
        if syntax.kind == SyntaxKind.WhileStatement:
            return self._bind_while_statement(syntax)
        assert False

    def _bind_block_statement(self, syntax) -> BoundStatement:
        statements = []
        self._scope = BoundScope(self._scope)
        for statement in syntax.statements:
            statement = self._bind_statement(statement)
            statements.append(statement)
        self._scope = cast(BoundScope, self._scope.parent)
        return BoundBlockStatement(tuple(statements))

    def _bind_expression_statement(self, syntax) -> BoundStatement:
        expression = self._bind_expression(syntax.expression)
        return BoundExpressionStatement(expression)

    def _bind_for_statement(self, syntax) -> BoundStatement:
        lower_bound = self._bind_expression(syntax.lower_bound, int)
        upper_bound = self._bind_expression(syntax.upper_bound, int)

        self._scope = BoundScope(self._scope)

        name = syntax.identifier_token.text
        variable = VariableSymbol(name, False, int)
        if not self._scope.try_declare(variable):
            self.diagnostics.report_variable_already_declared(
                syntax.identifier_token.span, name
            )
        body = self._bind_statement(syntax.body)

        self._scope = cast(BoundScope, self._scope.parent)
        return BoundForStatement(variable, lower_bound, upper_bound, body)

    def _bind_if_statement(self, syntax) -> BoundStatement:
        condition = self._bind_expression(syntax.condition, bool)
        then_statement = self._bind_statement(syntax.then_statement)
        if syntax.else_clause is None:
            else_statement = None
        else:
            else_statement = self._bind_statement(syntax.else_clause.else_statement)
        return BoundIfStatement(condition, then_statement, else_statement)

    def _bind_variable_declaration(self, syntax) -> BoundStatement:
        initializer = self._bind_expression(syntax.initializer)
        name = syntax.identifier.text
        is_read_only = syntax.keyword.kind == SyntaxKind.LetKeyword
        variable = VariableSymbol(name, is_read_only, initializer.type)
        if not self._scope.try_declare(variable):
            self.diagnostics.report_variable_already_declared(
                syntax.identifier.span, name
            )
        return BoundVariableDeclaration(variable, initializer)

    def _bind_while_statement(self, syntax) -> BoundStatement:
        condition = self._bind_expression(syntax.condition, bool)
        body = self._bind_statement(syntax.body)
        return BoundWhileStatement(condition, body)

    def _bind_expression(
        self, syntax: ExpressionSyntax, expected_type: Optional[Type] = None
    ) -> BoundExpression:
        if syntax.kind == SyntaxKind.AssignmentExpression:
            result = self._bind_assignment_expression(syntax)
        if syntax.kind == SyntaxKind.BinaryExpression:
            result = self._bind_binary_expression(syntax)
        if syntax.kind == SyntaxKind.LiteralExpression:
            result = self._bind_literal_expression(syntax)
        if syntax.kind == SyntaxKind.NameExpression:
            result = self._bind_name_expression(syntax)
        if syntax.kind == SyntaxKind.ParenthesizedExpression:
            result = self._bind_parenthesized_expression(syntax)
        if syntax.kind == SyntaxKind.UnaryExpression:
            result = self._bind_unary_expression(syntax)
        if expected_type is not None and result.type != expected_type:
            self.diagnostics.report_cannot_convert(
                syntax.span, result.type.__name__, expected_type.__name__
            )
        return result

    def _bind_assignment_expression(self, syntax) -> BoundExpression:
        expression = self._bind_expression(syntax.expression)
        name = syntax.identifier_token.text
        variable = self._scope.try_lookup(name)
        if variable is None:
            self.diagnostics.report_undefined_name(syntax.identifier_token.span, name)
            return expression

        if variable.is_read_only:
            self.diagnostics.report_cannot_assign(syntax.equals_token.span, name)
            return expression

        if expression.type != variable.type:
            self.diagnostics.report_cannot_convert(
                syntax.expression.span, expression.type.__name__, variable.type.__name__
            )
            return expression
        return BoundAssignmentExpression(variable, expression)

    def _bind_binary_expression(self, syntax) -> BoundExpression:
        left = self._bind_expression(syntax.left)
        right = self._bind_expression(syntax.right)
        operator = BoundBinaryOperator.bind(
            syntax.operator_token.kind, left.type, right.type
        )
        if operator is None:
            self.diagnostics.report_undefined_binary_operator(
                syntax.operator_token.span,
                syntax.operator_token.text,
                left.type.__name__,
                right.type.__name__,
            )
            return left
        return BoundBinaryExpression(left, operator, right)

    def _bind_literal_expression(self, syntax) -> BoundExpression:
        return BoundLiteralExpression(syntax.value)

    def _bind_name_expression(self, syntax) -> BoundExpression:
        name = syntax.identifier_token.text
        if name == "":
            return BoundLiteralExpression(0)
        variable = self._scope.try_lookup(name)
        if variable is None:
            self.diagnostics.report_undefined_name(syntax.identifier_token.span, name)
            return BoundLiteralExpression(0)
        return BoundVariableExpression(variable)

    def _bind_parenthesized_expression(self, syntax) -> BoundExpression:
        return self._bind_expression(syntax.expression)

    def _bind_unary_expression(self, syntax) -> BoundExpression:
        operand = self._bind_expression(syntax.operand)
        operator = BoundUnaryOperator.bind(syntax.operator_token.kind, operand.type)
        if operator is None:
            self.diagnostics.report_undefined_unary_operator(
                syntax.operator_token.span,
                syntax.operator_token.text,
                operand.type.__name__,
            )
            return operand
        return BoundUnaryExpression(operator, operand)
