from typing import Generator, Optional

from minsk.code_analysis.variable_symbol import VariableSymbol


class BoundScope:
    _variables: dict[str, VariableSymbol]
    parent: Optional["BoundScope"]

    def __init__(self, parent: Optional["BoundScope"]):
        self._variables = {}
        self.parent = parent

    def try_declare(self, variable: VariableSymbol) -> bool:
        if variable.name in self._variables:
            return False
        self._variables[variable.name] = variable
        return True

    def try_lookup(self, name: str) -> Optional[VariableSymbol]:
        variable = self._variables.get(name, None)
        if variable is not None:
            return variable

        if self.parent is not None:
            return self.parent.try_lookup(name)

        return None

    def get_declared_variables(self) -> Generator[VariableSymbol, None, None]:
        yield from self._variables.values()
