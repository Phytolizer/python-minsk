from abc import abstractmethod
from typing import Type

from minsk.code_analysis.binding.bound_node import BoundNode
from minsk.code_analysis.binding.bound_node_kind import BoundNodeKind


class BoundExpression(BoundNode):
    @property
    @abstractmethod
    def type(self) -> Type:
        pass


class BoundAssignmentExpression(BoundExpression):
    def __init__(self, variable, expression):
        self.variable = variable
        self.expression = expression

    @property
    def kind(self):
        return BoundNodeKind.AssignmentExpression

    @property
    def type(self):
        return self.expression.type


class BoundBinaryExpression(BoundExpression):
    def __init__(self, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right

    @property
    def kind(self):
        return BoundNodeKind.BinaryExpression

    @property
    def type(self):
        return self.operator.result_type


class BoundLiteralExpression(BoundExpression):
    def __init__(self, value):
        self.value = value

    @property
    def kind(self):
        return BoundNodeKind.LiteralExpression

    @property
    def type(self):
        return type(self.value)


class BoundUnaryExpression(BoundExpression):
    def __init__(self, operator, operand):
        self.operator = operator
        self.operand = operand

    @property
    def kind(self):
        return BoundNodeKind.UnaryExpression

    @property
    def type(self):
        return self.operator.result_type


class BoundVariableExpression(BoundExpression):
    def __init__(self, variable):
        self.variable = variable

    @property
    def kind(self):
        return BoundNodeKind.VariableExpression

    @property
    def type(self):
        return self.variable.type
