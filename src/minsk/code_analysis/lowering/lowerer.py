from minsk.code_analysis.binding.bound_expression import (
    BoundAssignmentExpression,
    BoundBinaryExpression,
    BoundLiteralExpression,
    BoundVariableExpression,
)
from minsk.code_analysis.binding.bound_statement import (
    BoundBlockStatement,
    BoundConditionalGotoStatement,
    BoundExpressionStatement,
    BoundGotoStatement,
    BoundLabelStatement,
    BoundVariableDeclaration,
    BoundWhileStatement,
)
from minsk.code_analysis.binding.bound_tree_rewriter import BoundTreeRewriter
from minsk.code_analysis.binding.operators import BoundBinaryOperator
from minsk.code_analysis.label_symbol import LabelSymbol
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.variable_symbol import VariableSymbol


class Lowerer(BoundTreeRewriter):
    def __init__(self):
        self._label_count = 0

    @staticmethod
    def lower(statement):
        lowerer = Lowerer()
        result = lowerer.rewrite_statement(statement)
        return Lowerer._flatten(result)

    def _generate_label(self):
        name = f"Label{self._label_count}"
        self._label_count += 1
        return LabelSymbol(name)

    @staticmethod
    def _flatten(statement):
        result = []
        stack = [statement]

        while stack:
            current = stack.pop()
            if isinstance(current, BoundBlockStatement):
                for s in reversed(current.statements):
                    stack.append(s)
            else:
                result.append(current)

        return BoundBlockStatement(tuple(result))

    def rewrite_for_statement(self, node):
        variable_declaration = BoundVariableDeclaration(node.variable, node.lower_bound)
        variable_expression = BoundVariableExpression(node.variable)
        upper_bound_symbol = VariableSymbol("upperBound", True, int)
        upper_bound_declaration = BoundVariableDeclaration(
            upper_bound_symbol, node.upper_bound
        )
        condition = BoundBinaryExpression(
            variable_expression,
            BoundBinaryOperator.bind(SyntaxKind.LessOrEqualsToken, int, int),
            BoundVariableExpression(upper_bound_symbol),
        )
        increment = BoundExpressionStatement(
            BoundAssignmentExpression(
                node.variable,
                BoundBinaryExpression(
                    variable_expression,
                    BoundBinaryOperator.bind(SyntaxKind.PlusToken, int, int),
                    BoundLiteralExpression(1),
                ),
            )
        )
        while_body = BoundBlockStatement((node.body, increment))
        while_statement = BoundWhileStatement(condition, while_body)
        result = BoundBlockStatement(
            (upper_bound_declaration, variable_declaration, while_statement)
        )
        return self.rewrite_statement(result)

    def rewrite_if_statement(self, node):
        # if <condition>
        #     <then>
        #
        # --->
        #
        # @<end>
        # goto_if_false <condition>
        # <then>
        # <end>:
        #
        # ============================
        #
        # if <condition>
        #     <then>
        # else
        #     <else>
        #
        # --->
        #
        # @<elselabel>
        # goto_if_false <condition>
        # <then>
        # @<end>
        # goto
        # <elselabel>:
        # <else>
        # <end>:
        if node.else_statement is None:
            end_label = self._generate_label()
            goto_false = BoundConditionalGotoStatement(end_label, node.condition, False)
            end_label_statement = BoundLabelStatement(end_label)
            result = BoundBlockStatement(
                (goto_false, node.then_statement, end_label_statement)
            )
            return self.rewrite_statement(result)
        else_label = self._generate_label()
        end_label = self._generate_label()
        goto_false_statement = BoundConditionalGotoStatement(
            else_label, node.condition, False
        )
        goto_end_statement = BoundGotoStatement(end_label)
        else_label_statement = BoundLabelStatement(else_label)
        end_label_statement = BoundLabelStatement(end_label)
        result = BoundBlockStatement(
            (
                goto_false_statement,
                node.then_statement,
                goto_end_statement,
                else_label_statement,
                node.else_statement,
                end_label_statement,
            )
        )
        return self.rewrite_statement(result)

    def rewrite_while_statement(self, node):
        # while <condition>
        #     <body>
        #
        # --->
        #
        # while:
        # goto_false :end <condition>
        # <body>
        # goto :while
        # end:
        continue_label = self._generate_label()
        end_label = self._generate_label()
        continue_label_statement = BoundLabelStatement(continue_label)
        goto_false_statement = BoundConditionalGotoStatement(
            end_label, node.condition, False
        )
        continue_statement = BoundGotoStatement(continue_label)
        end_label_statement = BoundLabelStatement(end_label)
        result = BoundBlockStatement(
            (
                continue_label_statement,
                goto_false_statement,
                node.body,
                continue_statement,
                end_label_statement,
            )
        )
        return self.rewrite_statement(result)
