from minsk.code_analysis.diagnostic import Diagnostic
from minsk.code_analysis.text.text_span import TextSpan


class DiagnosticBag:
    def __init__(self, initial=None):
        self._diagnostics = [] if initial is None else list(initial)

    def __iter__(self):
        return iter(self._diagnostics)

    def _report(self, span, message):
        self._diagnostics.append(Diagnostic(span, message))

    def report_invalid_integer(self, span, text):
        message = f"The text '{text}' doesn't represent a valid integer."
        self._report(span, message)

    def report_bad_character(self, position, character):
        message = f"Bad character in input: '{character}'."
        self._report(TextSpan(position, 1), message)

    def report_unexpected_token(self, span, expected, actual):
        message = f"Unexpected token <{actual}>, expected <{expected}>."
        self._report(span, message)

    def report_undefined_binary_operator(self, span, operator, left_type, right_type):
        message = (
            f"The binary operator '{operator}' is not defined "
            + f"for types '{left_type}' and '{right_type}'."
        )
        self._report(span, message)

    def report_undefined_unary_operator(self, span, operator, operand_type):
        message = (
            f"The unary operator '{operator}' is "
            + f"not defined for type '{operand_type}'."
        )
        self._report(span, message)

    def report_undefined_name(self, span, name):
        message = f"Variable '{name}' doesn't exist."
        self._report(span, message)

    def report_variable_already_declared(self, span, name):
        message = f"Variable '{name}' has already been declared."
        self._report(span, message)

    def report_cannot_convert(self, span, from_type, to_type):
        message = f"Cannot convert '{from_type}' to '{to_type}'."
        self._report(span, message)

    def report_cannot_assign(self, span, name):
        message = f"Variable '{name}' is read-only and cannot be assigned to."
        self._report(span, message)
