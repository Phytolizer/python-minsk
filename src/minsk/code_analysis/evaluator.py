from typing import Any

from minsk.code_analysis.binding.bound_expression import (
    BoundAssignmentExpression,
    BoundBinaryExpression,
    BoundLiteralExpression,
    BoundUnaryExpression,
    BoundVariableExpression,
)
from minsk.code_analysis.binding.bound_node_kind import BoundNodeKind
from minsk.code_analysis.binding.bound_statement import (
    BoundBlockStatement,
    BoundLabelStatement,
)
from minsk.code_analysis.binding.operators import (
    BoundBinaryOperatorKind,
    BoundUnaryOperatorKind,
)
from minsk.code_analysis.label_symbol import LabelSymbol
from minsk.code_analysis.variable_symbol import VariableSymbol


class Evaluator:
    _last_value: Any

    def __init__(self, root: BoundBlockStatement, variables: dict[VariableSymbol, Any]):
        self._root = root
        self._variables = variables
        self._last_value = None

    def evaluate(self):
        label_to_index: dict[LabelSymbol, int] = {}
        for (i, s) in enumerate(self._root.statements):
            if isinstance(s, BoundLabelStatement):
                label_to_index[s.label] = i + 1
        index = 0
        while index < len(self._root.statements):
            statement = self._root.statements[index]
            if statement.kind == BoundNodeKind.ConditionalGotoStatement:
                condition = self._evaluate_expression(statement.condition)
                if condition == statement.jump_if_true:
                    index = label_to_index[statement.label]
                else:
                    index += 1
            elif statement.kind == BoundNodeKind.ExpressionStatement:
                self._evaluate_expression_statement(statement)
                index += 1
            elif statement.kind == BoundNodeKind.GotoStatement:
                index = label_to_index[statement.label]
            elif statement.kind == BoundNodeKind.VariableDeclaration:
                self._evaluate_variable_declaration(statement)
                index += 1
            elif statement.kind == BoundNodeKind.LabelStatement:
                index += 1
            else:
                raise ValueError(f"unhandled node {statement.kind.name}")
        return self._last_value

    def _evaluate_expression_statement(self, node):
        self._last_value = self._evaluate_expression(node.expression)

    def _evaluate_variable_declaration(self, node):
        value = self._evaluate_expression(node.initializer)
        self._variables[node.variable] = value
        self._last_value = value

    def _evaluate_expression(self, root):
        if root.kind == BoundNodeKind.AssignmentExpression:
            return self._evaluate_assignment_expression(root)
        if root.kind == BoundNodeKind.BinaryExpression:
            return self._evaluate_binary_expression(root)
        if root.kind == BoundNodeKind.LiteralExpression:
            return self._evaluate_literal_expression(root)
        if root.kind == BoundNodeKind.UnaryExpression:
            return self._evaluate_unary_expression(root)
        if root.kind == BoundNodeKind.VariableExpression:
            return self._evaluate_variable_expression(root)
        raise ValueError(f"Unhandled node {root}")

    def _evaluate_assignment_expression(self, node: BoundAssignmentExpression):
        value = self._evaluate_expression(node.expression)
        self._variables[node.variable] = value
        return value

    def _evaluate_binary_expression(self, node: BoundBinaryExpression):
        left = self._evaluate_expression(node.left)
        right = self._evaluate_expression(node.right)

        if node.operator.kind == BoundBinaryOperatorKind.Addition:
            return left + right
        if node.operator.kind == BoundBinaryOperatorKind.Subtraction:
            return left - right
        if node.operator.kind == BoundBinaryOperatorKind.Multiplication:
            return left * right
        if node.operator.kind == BoundBinaryOperatorKind.Division:
            return left / right
        if node.operator.kind == BoundBinaryOperatorKind.LogicalAnd:
            return left and right
        if node.operator.kind == BoundBinaryOperatorKind.LogicalOr:
            return left or right
        if node.operator.kind == BoundBinaryOperatorKind.Equality:
            return left == right
        if node.operator.kind == BoundBinaryOperatorKind.Inequality:
            return left != right
        if node.operator.kind == BoundBinaryOperatorKind.LessOrEquals:
            return left <= right
        if node.operator.kind == BoundBinaryOperatorKind.GreaterOrEquals:
            return left >= right
        if node.operator.kind == BoundBinaryOperatorKind.Less:
            return left < right
        if node.operator.kind == BoundBinaryOperatorKind.Greater:
            return left > right
        if node.operator.kind == BoundBinaryOperatorKind.BitwiseAnd:
            return left & right
        if node.operator.kind == BoundBinaryOperatorKind.BitwiseOr:
            return left | right
        if node.operator.kind == BoundBinaryOperatorKind.BitwiseXor:
            return left ^ right

    def _evaluate_literal_expression(self, node: BoundLiteralExpression):
        return node.value

    def _evaluate_unary_expression(self, node: BoundUnaryExpression):
        operand = self._evaluate_expression(node.operand)
        if node.operator.kind == BoundUnaryOperatorKind.Identity:
            return operand
        if node.operator.kind == BoundUnaryOperatorKind.Negation:
            return -operand
        if node.operator.kind == BoundUnaryOperatorKind.LogicalNegation:
            return not operand
        if node.operator.kind == BoundUnaryOperatorKind.BitwiseNegation:
            return ~operand

    def _evaluate_variable_expression(self, node: BoundVariableExpression):
        return self._variables[node.variable]
