from dataclasses import dataclass
from itertools import chain
from typing import Any, Optional

from minsk.code_analysis.binding.binder import Binder
from minsk.code_analysis.binding.bound_global_scope import BoundGlobalScope
from minsk.code_analysis.evaluator import Evaluator
from minsk.code_analysis.lowering.lowerer import Lowerer
from minsk.code_analysis.syntax.syntax_tree import SyntaxTree
from minsk.code_analysis.variable_symbol import VariableSymbol


@dataclass
class EvaluationResult:
    value: Any
    diagnostics: tuple


class Compilation:
    _previous: Optional["Compilation"]
    _global_scope: Optional[BoundGlobalScope]
    syntax: SyntaxTree

    def __init__(self, syntax: SyntaxTree, previous: Optional["Compilation"] = None):
        self._previous = previous
        self._global_scope = None
        self.syntax = syntax

    @property
    def global_scope(self) -> BoundGlobalScope:
        if self._global_scope is None:
            if self._previous is None:
                previous_global_scope = None
            else:
                previous_global_scope = self._previous.global_scope
            self._global_scope = Binder.bind_global_scope(
                previous_global_scope, self.syntax.root
            )
        return self._global_scope

    def continue_with(self, syntax: SyntaxTree) -> "Compilation":
        return Compilation(syntax, self)

    def evaluate(self, variables: dict[VariableSymbol, Any]) -> EvaluationResult:
        diagnostics = tuple(
            chain(self.syntax.diagnostics, self.global_scope.diagnostics)
        )
        if diagnostics:
            return EvaluationResult(None, diagnostics)

        evaluator = Evaluator(self._get_statement(), variables)
        value = evaluator.evaluate()
        return EvaluationResult(value, ())

    def _get_statement(self):
        result = self.global_scope.statement
        return Lowerer.lower(result)

    def emit_tree(self, console):
        self._get_statement().pretty_print(console)
