from minsk.code_analysis.text.text_span import TextSpan


class Diagnostic:
    def __init__(self, span: TextSpan, message: str):
        self.span = span
        self.message = message

    def __str__(self):
        return self.message

    def __repr__(self):
        return self.message
