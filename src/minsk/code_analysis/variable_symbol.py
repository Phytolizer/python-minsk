from dataclasses import dataclass
from typing import Type


@dataclass
class VariableSymbol:
    name: str
    is_read_only: bool
    type: Type

    def __hash__(self):
        return hash((self.name, self.type.__name__))

    def __str__(self):
        return self.name
