from typing import Any, Generator

import pytest

from minsk.code_analysis.compilation import Compilation
from minsk.code_analysis.syntax.syntax_tree import SyntaxTree
from minsk.code_analysis.text.text_span import TextSpan
from minsk.code_analysis.variable_symbol import VariableSymbol


class AnnotatedText:
    def __init__(self, text, spans):
        self.text = text
        self.spans = spans

    @staticmethod
    def parse(text) -> "AnnotatedText":
        text = AnnotatedText.unindent(text)

        text_builder = ""
        span_builder = []
        stack = []
        position = 0

        for c in text:
            if c == "[":
                stack.append(position)
            elif c == "]":
                span = TextSpan.from_bounds(stack.pop(), position)
                span_builder.append(span)
            else:
                text_builder += c
                position += 1

        return AnnotatedText(text_builder, tuple(span_builder))

    @staticmethod
    def unindent(text: str) -> str:
        return "\n".join(AnnotatedText.unindent_lines(text))

    @staticmethod
    def unindent_lines(text: str) -> list[str]:
        lines = list(text.splitlines())
        min_indentation = -1
        for line in lines:
            if len(line.strip()) == 0:
                continue
            indentation = len(line) - len(line.lstrip())
            if min_indentation == -1 or indentation < min_indentation:
                min_indentation = indentation
        return [
            line[min_indentation:] for line in lines if len(line) >= min_indentation
        ]


def assert_diagnostics(text: str, diagnostics: str):
    annotated_text = AnnotatedText.parse(text)
    expected_diagnostics = AnnotatedText.unindent_lines(diagnostics)
    expected_spans = annotated_text.spans
    print(repr(expected_diagnostics))
    assert len(expected_diagnostics) == len(expected_spans)

    tree = SyntaxTree.parse(annotated_text.text)
    compilation = Compilation(tree)
    variables: dict[VariableSymbol, Any] = {}
    result = compilation.evaluate(variables)
    assert len(result.diagnostics) == len(expected_diagnostics)
    for (expected_message, actual_diagnostic, expected_span) in zip(
        expected_diagnostics, result.diagnostics, expected_spans
    ):
        assert actual_diagnostic.message == expected_message
        assert actual_diagnostic.span.start == expected_span.start
        assert actual_diagnostic.span.end == expected_span.end


def get_evaluator_value_tests() -> Generator[tuple[str, Any], None, None]:
    yield ("1", 1)
    yield ("+1", 1)
    yield ("-1", -1)
    yield ("1 + 2", 3)
    yield ("1 - 2", -1)
    yield ("12 * 2", 24)
    yield ("9 / 3", 3)
    yield ("true", True)
    yield ("false", False)
    yield ("true && false", False)
    yield ("false && true", False)
    yield ("false && false", False)
    yield ("true && true", True)
    yield ("true || false", True)
    yield ("false || true", True)
    yield ("false || false", False)
    yield ("true || true", True)
    yield ("!true", False)
    yield ("!false", True)
    yield ("!!true", True)
    yield ("1 == 2", False)
    yield ("1 != 2", True)
    yield ("true == false", False)
    yield ("false == false", True)
    yield ("1 < 2", True)
    yield ("1 < 1", False)
    yield ("1 < 0", False)
    yield ("1 <= 2", True)
    yield ("1 <= 1", True)
    yield ("1 <= 0", False)
    yield ("1 > 2", False)
    yield ("1 > 1", False)
    yield ("1 > 0", True)
    yield ("1 >= 2", False)
    yield ("1 >= 1", True)
    yield ("1 >= 0", True)
    yield ("1 | 2", 3)
    yield ("1 | 1", 1)
    yield ("1 & 2", 0)
    yield ("3 & 2", 2)
    yield ("1 ^ 3", 2)
    yield ("~1", -2)
    yield ("{ var a = 0 (a = 10) * a }", 100)
    yield ("{ var a = 0 if a == 0 a = 10 a }", 10)
    yield ("{ var a = 0 if a == 4 a = 10 a }", 0)
    yield ("{ var a = 0 if a == 4 a = 10 else a = 20 a }", 20)
    yield ("{ var a = 0 if a == 0 a = 10 else a = 20 a }", 10)
    yield (
        """
        {
            var i = 10
            var result = 0
            while i > 0
            {
                result = result + i
                i = i - 1
            }
            result
        }
        """,
        55,
    )
    yield (
        """
        {
            var result = 0
            for i = 1 to 10
            {
                result = result + i
            }
            result
        }
        """,
        55,
    )


@pytest.mark.parametrize("text,expected", get_evaluator_value_tests())
def test_computes_correct_value(text, expected):
    tree = SyntaxTree.parse(text)
    compilation = Compilation(tree)
    variables = {}
    result = compilation.evaluate(variables)
    assert result.diagnostics == ()
    assert result.value == expected


def test_block_statement_no_infinite_loop():
    text = """
        {
        [)][]
    """
    diagnostics = """
        Unexpected token <CloseParenthesisToken>, expected <IdentifierToken>.
        Unexpected token <EndOfFileToken>, expected <CloseBraceToken>.
    """
    assert_diagnostics(text, diagnostics)


def test_variable_declaration_reports_redeclaration():
    text = """
    {
        var x = 10
        var y = 100
        {
            var x = 10
        }
        var [x] = 5
    }
    """
    diagnostics = """
        Variable 'x' has already been declared.
    """
    assert_diagnostics(text, diagnostics)


def test_name_expression_reports_undefined():
    text = "[x] * 10"
    diagnostics = """
        Variable 'x' doesn't exist.
    """
    assert_diagnostics(text, diagnostics)


def test_name_expression_reports_no_error_for_inserted_token():
    text = "[]"
    diagnostics = """
        Unexpected token <EndOfFileToken>, expected <IdentifierToken>.
    """
    assert_diagnostics(text, diagnostics)


def test_assignment_expression_reports_undefined():
    text = "[x] = 10"
    diagnostics = """
        Variable 'x' doesn't exist.
    """
    assert_diagnostics(text, diagnostics)


def test_assignment_expression_reports_cannot_assign():
    text = """
    {
        let x = 10
        x [=] 0
    }
    """
    diagnostics = """
        Variable 'x' is read-only and cannot be assigned to.
    """
    assert_diagnostics(text, diagnostics)


def test_assignment_expression_reports_cannot_convert():
    text = """
    {
        var x = 10
        x = [true]
    }
    """
    diagnostics = """
        Cannot convert 'bool' to 'int'.
    """
    assert_diagnostics(text, diagnostics)


def test_if_statement_reports_cannot_convert():
    text = """
    {
        var x = 0
        if [10]
            x = 10
    }
    """
    diagnostics = """
        Cannot convert 'int' to 'bool'.
    """
    assert_diagnostics(text, diagnostics)


def test_while_statement_reports_cannot_convert():
    text = """
    {
        var x = 0
        while [10]
            x = 10
    }
    """
    diagnostics = """
        Cannot convert 'int' to 'bool'.
    """
    assert_diagnostics(text, diagnostics)


def test_for_statement_reports_cannot_convert_lower_bound():
    text = """
    {
        var result = 0
        for i = [false] to 10
            result = result + i
    }
    """
    diagnostics = """
        Cannot convert 'bool' to 'int'.
    """
    assert_diagnostics(text, diagnostics)


def test_for_statement_reports_cannot_convert_upper_bound():
    text = """
    {
        var result = 0
        for i = 0 to [true]
            result = result + i
    }
    """
    diagnostics = """
        Cannot convert 'bool' to 'int'.
    """
    assert_diagnostics(text, diagnostics)


def test_unary_expression_reports_undefined():
    text = "[+]true"
    diagnostics = """
        The unary operator '+' is not defined for type 'bool'.
    """
    assert_diagnostics(text, diagnostics)


def test_binary_expression_reports_undefined():
    text = "true [*] 3"
    diagnostics = """
        The binary operator '*' is not defined for types 'bool' and 'int'.
    """
    assert_diagnostics(text, diagnostics)
