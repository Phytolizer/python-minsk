import pytest

from minsk.code_analysis.syntax import syntax_facts
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_tree import SyntaxTree


@pytest.mark.parametrize(
    "kind", filter(lambda k: syntax_facts.get_text(k) is not None, SyntaxKind)
)
def test_get_text_round_trips(kind):
    text = syntax_facts.get_text(kind)
    tokens = SyntaxTree.parse_tokens(text)
    assert len(tokens) == 1
    assert tokens[0].kind == kind
    assert tokens[0].text == text
