from itertools import chain
from typing import Generator

import pytest

from minsk.code_analysis.syntax import syntax_facts
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_tree import SyntaxTree


def get_tokens() -> Generator[tuple[SyntaxKind, str], None, None]:
    dynamic_tokens = (
        (SyntaxKind.IdentifierToken, "a"),
        (SyntaxKind.IdentifierToken, "abc"),
        (SyntaxKind.NumberToken, "1"),
        (SyntaxKind.NumberToken, "123"),
    )
    for tup in dynamic_tokens:
        yield tup
    for kind in SyntaxKind:
        token_text = syntax_facts.get_text(kind)
        if token_text is not None:
            yield (kind, token_text)


def get_separators() -> tuple[tuple[SyntaxKind, str], ...]:
    return (
        (SyntaxKind.WhitespaceToken, " "),
        (SyntaxKind.WhitespaceToken, "  "),
        (SyntaxKind.WhitespaceToken, "\r"),
        (SyntaxKind.WhitespaceToken, "\n"),
        (SyntaxKind.WhitespaceToken, "\r\n"),
    )


def requires_separator(t1kind: SyntaxKind, t2kind: SyntaxKind) -> bool:
    t1_is_keyword = t1kind.name.endswith("Keyword")
    t2_is_keyword = t2kind.name.endswith("Keyword")
    if (t1_is_keyword or t1kind == SyntaxKind.IdentifierToken) and (
        t2_is_keyword or t2kind in {SyntaxKind.IdentifierToken, SyntaxKind.NumberToken}
    ):
        return True
    if t1kind == SyntaxKind.NumberToken and t2kind == SyntaxKind.NumberToken:
        return True
    if t1kind in {
        SyntaxKind.EqualsToken,
        SyntaxKind.BangToken,
        SyntaxKind.LessToken,
        SyntaxKind.GreaterToken,
    } and t2kind in {
        SyntaxKind.EqualsToken,
        SyntaxKind.EqualsEqualsToken,
    }:
        return True
    if t1kind == SyntaxKind.AmpersandToken and t2kind in {
        SyntaxKind.AmpersandToken,
        SyntaxKind.AmpersandAmpersandToken,
    }:
        return True
    if t1kind == SyntaxKind.PipeToken and t2kind in {
        SyntaxKind.PipeToken,
        SyntaxKind.PipePipeToken,
    }:
        return True
    return False


def get_token_pairs() -> Generator[tuple[SyntaxKind, str, SyntaxKind, str], None, None]:
    for (t1kind, t1text) in get_tokens():
        for (t2kind, t2text) in get_tokens():
            if not requires_separator(t1kind, t2kind):
                yield (t1kind, t1text, t2kind, t2text)


def get_token_pairs_with_separator() -> Generator[
    tuple[SyntaxKind, str, SyntaxKind, str, SyntaxKind, str], None, None
]:
    for (t1kind, t1text) in get_tokens():
        for (t2kind, t2text) in get_tokens():
            if requires_separator(t1kind, t2kind):
                for (sep_kind, sep_text) in get_separators():
                    yield (t1kind, t1text, t2kind, t2text, sep_kind, sep_text)


def test_lexer_tests_all_tokens():
    all_kinds = set(
        filter(
            lambda kind: kind.name.endswith("Token") or kind.name.endswith("Keyword"),
            SyntaxKind,
        )
    )
    tested_kinds = set(map(lambda t: t[0], chain(get_tokens(), get_separators())))
    difference = all_kinds - tested_kinds
    assert difference - {SyntaxKind.BadToken, SyntaxKind.EndOfFileToken} == set()


@pytest.mark.parametrize("kind,text", get_tokens())
def test_lexer_lexes_token(kind, text):
    tokens = SyntaxTree.parse_tokens(text)
    assert len(tokens) == 1
    assert tokens[0].kind == kind
    assert tokens[0].text == text


@pytest.mark.parametrize("t1kind,t1text,t2kind,t2text", get_token_pairs())
def test_lexer_lexes_token_pairs(t1kind, t1text, t2kind, t2text):
    text = f"{t1text}{t2text}"
    tokens = SyntaxTree.parse_tokens(text)
    assert len(tokens) == 2
    assert tokens[0].kind == t1kind
    assert tokens[0].text == t1text
    assert tokens[1].kind == t2kind
    assert tokens[1].text == t2text


@pytest.mark.parametrize(
    "t1kind,t1text,t2kind,t2text,sep_kind,sep_text", get_token_pairs_with_separator()
)
def test_lexer_lexes_token_pairs_with_separator(
    t1kind, t1text, t2kind, t2text, sep_kind, sep_text
):
    text = f"{t1text}{sep_text}{t2text}"
    tokens = SyntaxTree.parse_tokens(text)
    assert len(tokens) == 3
    assert tokens[0].kind == t1kind
    assert tokens[0].text == t1text
    assert tokens[1].kind == sep_kind
    assert tokens[1].text == sep_text
    assert tokens[2].kind == t2kind
    assert tokens[2].text == t2text
