from typing import Generator

import pytest

from minsk.code_analysis.syntax import syntax_facts
from minsk.code_analysis.syntax.expression_syntax import ExpressionSyntax
from minsk.code_analysis.syntax.statement_syntax import ExpressionStatementSyntax
from minsk.code_analysis.syntax.syntax_kind import SyntaxKind
from minsk.code_analysis.syntax.syntax_node import SyntaxNode
from minsk.code_analysis.syntax.syntax_token import SyntaxToken
from minsk.code_analysis.syntax.syntax_tree import SyntaxTree


def flatten(root) -> Generator[SyntaxNode, None, None]:
    stack = [root]
    while len(stack) > 0:
        node = stack.pop()
        yield node
        for child in reversed(tuple(node.get_children())):
            stack.append(child)


class AssertingIterator:
    def __init__(self, root):
        self.nodes = flatten(root)

    def assert_token(self, kind, text):
        node = next(self.nodes, None)
        assert node is not None
        assert isinstance(node, SyntaxToken)
        assert node.kind == kind
        assert node.text == text

    def assert_node(self, kind):
        node = next(self.nodes, None)
        assert node is not None
        assert not isinstance(node, SyntaxToken)
        assert node.kind == kind

    def finish(self):
        assert next(self.nodes, None) is None


def get_binary_operator_pairs() -> Generator[tuple[SyntaxKind, SyntaxKind], None, None]:
    for op1 in syntax_facts.get_binary_operators():
        for op2 in syntax_facts.get_binary_operators():
            yield (op1, op2)


def get_unary_operator_pairs() -> Generator[tuple[SyntaxKind, SyntaxKind], None, None]:
    for unary in syntax_facts.get_unary_operators():
        for binary in syntax_facts.get_binary_operators():
            yield (unary, binary)


def parse_expression(text) -> ExpressionSyntax:
    tree = SyntaxTree.parse(text)
    assert len(tree.diagnostics) == 0
    assert isinstance(tree.root.statement, ExpressionStatementSyntax)
    return tree.root.statement.expression


@pytest.mark.parametrize("op1,op2", get_binary_operator_pairs())
def test_binary_expression_honors_precedences(op1, op2):
    op1text = syntax_facts.get_text(op1)
    op2text = syntax_facts.get_text(op2)
    text = f"a {op1text} b {op2text} c"
    expression = parse_expression(text)

    op1prec = syntax_facts.binary_operator_precedence(op1)
    op2prec = syntax_facts.binary_operator_precedence(op2)

    e = AssertingIterator(expression)
    e.assert_node(SyntaxKind.BinaryExpression)
    if op1prec >= op2prec:
        e.assert_node(SyntaxKind.BinaryExpression)
        e.assert_node(SyntaxKind.NameExpression)
        e.assert_token(SyntaxKind.IdentifierToken, "a")
        e.assert_token(op1, op1text)
    else:
        e.assert_node(SyntaxKind.NameExpression)
        e.assert_token(SyntaxKind.IdentifierToken, "a")
        e.assert_token(op1, op1text)
        e.assert_node(SyntaxKind.BinaryExpression)
    e.assert_node(SyntaxKind.NameExpression)
    e.assert_token(SyntaxKind.IdentifierToken, "b")
    e.assert_token(op2, op2text)
    e.assert_node(SyntaxKind.NameExpression)
    e.assert_token(SyntaxKind.IdentifierToken, "c")
    e.finish()


@pytest.mark.parametrize("unary,binary", get_unary_operator_pairs())
def test_unary_expression_honors_precedence(unary, binary):
    unary_text = syntax_facts.get_text(unary)
    binary_text = syntax_facts.get_text(binary)
    text = f"{unary_text} a {binary_text} b"
    expression = parse_expression(text)

    unary_prec = syntax_facts.unary_operator_precedence(unary)
    binary_prec = syntax_facts.binary_operator_precedence(binary)

    e = AssertingIterator(expression)
    if unary_prec >= binary_prec:
        e.assert_node(SyntaxKind.BinaryExpression)
        e.assert_node(SyntaxKind.UnaryExpression)
        e.assert_token(unary, unary_text)
        e.assert_node(SyntaxKind.NameExpression)
        e.assert_token(SyntaxKind.IdentifierToken, "a")
        e.assert_token(binary, binary_text)
        e.assert_node(SyntaxKind.NameExpression)
        e.assert_token(SyntaxKind.IdentifierToken, "b")
    else:
        e.assert_node(SyntaxKind.UnaryExpression)
        e.assert_token(unary, unary_text)
        e.assert_node(SyntaxKind.BinaryExpression)
        e.assert_node(SyntaxKind.NameExpression)
        e.assert_token(SyntaxKind.IdentifierToken, "a")
        e.assert_token(binary, binary_text)
        e.assert_node(SyntaxKind.NameExpression)
        e.assert_token(SyntaxKind.IdentifierToken, "b")
    e.finish()
